class Field {
  constructor() {
    var [
      name = 'name', 
      type = 'integer', 
      value = null, 
      ...params
    ] = Array.from(arguments);

    this.name = name;
    this.type = type;
    this.params = params;
    this.value = value;

  }

  valueOf() {
    return [this.name, this.type, ...this.params];
  }
}

module.exports = Field;
