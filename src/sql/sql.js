class Sql {
  constructor(model) {
    this.model = model;
    this._sql = '';
    this._params = [];
  }

  params() {
    return this._params;
  }

  toString() {
    return this._sql;
  }
}

module.exports = Sql;
