function forObj(obj, callback) {
  for(var key in obj) {
    callback(key, obj[key]);
  }
}

module.exports = forObj;
