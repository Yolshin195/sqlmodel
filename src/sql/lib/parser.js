function parser(key, value) {
  if(value instanceof Array) {
    return `${key} in (${value})`;
  }
  
  return key + ' ' + value;
}

module.exports = parser;
