const Create = require('./create');
const Select = require('./select');
const Insert = require('./insert');
const Update = require('./update');
const Delete = require('./delete');
const CustomSql = require('./customSql');

module.exports = {
  Create,
  Select,
  Insert,
  Update,
  Delete,
  CustomSql
}
