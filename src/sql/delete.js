const Sql = require('./sql.js');
const where = require('./lib/where');

class Delete extends Sql {
  constructor(model, {where}) {
    super(model);

    this.filter = where || {}

    this.sql();
  }

  /*
   * @param {Object} 
   * @return {Array}
   */
  where(obj) {
    return where(obj);
  }

  sql() {
    this._sql = 'DELETE FROM ' + this.model.name + ' ' + this.where(this.filter) +';';
  }
}

function forObj(obj, callback) {
  for(var key in obj) {
    callback(key, obj[key]);
  }
}

module.exports = Delete;
