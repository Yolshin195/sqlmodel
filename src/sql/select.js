const Sql = require('./sql.js');
const where = require('./lib/where');
const parser = require('./lib/parser');
const forObj = require('./lib/froObj');

class Select extends Sql {

  constructor(model, obj = {}) {
    super(model);
      this.filter = {
        select: '*', 
        from: model.name,
        ...obj
      }
    }
  
  /*
   * @param {Object} 
   * @return {Array}
   */
  where(obj) {
    return where(obj);
  }

  toString() {
    let arr = [];

    forObj(this.filter, (key, value) => {
      switch(key) {
        case 'where':
          return arr.push(this.where(value));

        default:
          return arr.push(parser(key, value));
      }   
    });
    
    return arr.join(' ') + ';';
  }
}

module.exports = Select;
