class Model {
  constructor() {
    this.name = null;
    this.fields = [];
  }

  /*
   * @param {object}
   * @return {object}
   */
  create() {
    var obj = new Object(null);
    var arg = Array.from(arguments);
    var filter =  this.fields
        .filter( el => el.value !== 'undefined' )
    
    if( typeof arg[0] === 'object' ) {
      filter
        .forEach((el, i) => {
          obj[el.name] = arg[0][el.name] || el.value;
        });

      return obj;
    }

    filter
      .forEach((el, i) => {
        obj[el.name] = arg[i] || el.value;
      });

    return obj;
  }

  /*
   * @param {Array of Object} value
   * @return {Array of Array}
   */
  paramsMulti(value) {
    return value.map((el, id) => this.fields
      .filter( el => el.value !== 'undefined' )
      .map( el => (value[id].hasOwnProperty(el.name)) ?
        value[id][el.name] : el.value )
    );
  }


  /*
   * @param {Object} value
   * @return {Array}
   */
  params(value) {
    return this.fields
      .filter( el => el.value !== 'undefined' )
      .map( el => (value.hasOwnProperty(el.name)) ?
        value[el.name] : el.value
      );
  }
}

module.exports = Model;
